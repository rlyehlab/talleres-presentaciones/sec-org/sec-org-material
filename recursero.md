# Recursero de herramientas y guías

Descargar versión actualizada del recursero desde:
https://pad.kefir.red/p/linklank

## Seguridad digital feminista
Principios feministas de Internet: https://feministinternet.org/sites/default/files/Principios_feministas_para_internetv2-0.pdf

## Escenarios de seguridad
https://ssd.eff.org/es/module-categories/escenarios-de-seguridad
https://www.digitalfirstaid.org/es/

## Cómo funciona internet
https://protege.la/wp-content/uploads/2018/06/0011_guia_ComoFuncionaInternet_MiniCheckl.pdf
https://myshadow.org/ckeditor_assets/attachments/264/es_howtheinternetworks.pdf
Video: https://www.youtube.com/watch?v=SYDWC-qr8mo&feature=youtu.be

## Modelo de amenazas y Análisis de riesgos:
https://sec.eff.org/topics/threat-modeling
https://ssd.eff.org/es/module/evaluando-tus-riesgos

## Herramientas
Como elegir herramientas: https://myshadow.org/ckeditor_assets/attachments/251/escogerherramientas.pdf

### Navegación anónima
Red Tor
https://torproject.org
Video: https://www.youtube.com/watch?v=Sz_J6vJ4MYw&index=4&t=0s&list=PLwyU2dZ3LJErtu3GGElIa7VyORE2B6H1H

Guía de Navegador Tor: 
Windows: https://securityinabox.org/es/guide/torbrowser/windows/
Linux: https://securityinabox.org/es/guide/torbrowser/linux/

### Navegación segura
Complementos del navegador
- HTTPS Everywhere (por defecto en el navegador Tor)
	Mozilla: https://addons.mozilla.org/es/firefox/addon/https-everywhere/
	Chrome: https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp?hl=es
- NoScript (por defecto en el navegador Tor)
	Mozilla: https://addons.mozilla.org/es/firefox/addon/noscript/
	Chrome: https://chrome.google.com/webstore/detail/noscript/doojmbjmlfjjnbmnoijecmcbfeoakpjm
- uBlock Origin
	Mozilla: https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/
	Chrome: https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=es

## Navegacion segura
### VPN
- Qué es una VPN? (inglés) https://cdt.org/blog/techsplanations-part-5-virtual-private-networks/
- Cómo elegir una VPN?
https://ssd.eff.org/es/playlist/%C2%BFdefensora-de-derechos-humanos#escogiendo-el-vpn-apropiado-para-usted
Bitmask (gratuita. Android)
Proton VPN (paga. Computadora)
NordVPN (paga)

## Redes sociales y ofuscación de identidades
https://ssd.eff.org/es/playlist/%C2%BFqui%C3%A9res-un-kit-b%C3%A1sico-de-seguridad#protegi%C3%A9ndose-en-las-redes-sociales

### Contraseñas seguras
https://securityinabox.org/es/guide/passwords/
https://ssd.eff.org/es/playlist/%C2%BFqui%C3%A9res-un-kit-b%C3%A1sico-de-seguridad#creando-contrase%C3%B1as-seguras

### Gestores de contraseñas
KeePassX | Windows Mac Linux

https://sec.eff.org/topics/password-managers
https://securityinabox.org/es/guide/keepassx/windows/
https://securityinabox.org/es/guide/keepassx/linux/

KeepassDroid | Android **(agregado en base a consultas!)**

https://securityinabox.org/es/guide/keepassdroid/android/

(ese es el recomendable en Android porque tiene mayor número de descargas y lo han recomendado en Security in a Box que es un sitio de confianza)

## Almacenar información sensible

### Guías
https://ssd.eff.org/es/playlist/%C2%BFqui%C3%A9res-un-kit-b%C3%A1sico-de-seguridad#manteniendo-sus-datos-seguros

#### Cifrado completo de disco
Gnu/Linux: al instalar el sistema operativo ofrece opción de cifrar disco
Bitlocker (Windows "pro"): https://www.muycomputer.com/2017/09/26/bitlocker-en-windows-10-2/
FileVault (Mac): https://ssd.eff.org/es/module/c%C3%B3mo-cifrar-su-iphone

#### Cifrado de carpetas
Veracrypt: https://proprivacy.com/guides/veracrypt-how-to-basics

### Cifrado punto a punto
https://ssd.eff.org/es/module/una-mirada-en-profundidad-al-cifrado-de-extremo-extremo-%C2%BFc%C3%B3mo-funcionan-los-sistemas-de
#### Cifrado de celulares
Samsung (ingles) https://www.samsung.com/uk/support/mobile-devices/what-is-the-secure-folder-and-how-do-i-use-it/
Huawei: opción Seguridad / Caja fuerte

## Cómo nos comunicamos
https://ssd.eff.org/es/playlist/%C2%BFdefensora-de-derechos-humanos#comunic%C3%A1ndote-con-otros

### Uso de celulares
Buenas prácticas https://securityinabox.org/en/guide/smartphones/
https://myshadow.org/ckeditor_assets/attachments/229/mobile_es_complete.pdf

#### Apps de comunicaciones seguras
Comparativa: https://www.securemessagingapps.com/

Signal https://securityinabox.org/es/guide/signal/android/
Wire secure messanger (disponible en google play) 

### Correos electrónicos alternativos
Protonmail: https://protonmail.com/

Disposable emails: 
guerrillamail https://www.guerrillamail.com/es/
Temp mail: https://temp-mail.org/es/

### Encriptar correos
GPG: 
https://ssd.eff.org/es/module/como-usar-pgp-para-windows-pc (Windows)
https://ssd.eff.org/es/module/como-usar-pgp-para-linux  (Linux)

### Compartir archivos
Share riseup: https://share.riseup.net/

## Evitar la infección con virus

¿Qué es un antivirus? https://ssd.eff.org/es/glossary/antivirus

Avast: https://securityinabox.org/es/guide/avast/windows/ (permite actualizaciones)

Avira https://www.avira.com/es/free-antivirus-windows o AVG https://www.avg.com/es-ar/homepage

ClamWin https://level-up.cc/curriculum/malware-protection/using-antivirus-tools/deepening/using-antivirus-tools/ (es portable es USB)



## Organizaciones aliadas

### Estrategias de Autodefensa digital
EFF
https://www.eff.org
Security in a box
https://securityinabox.org/es/
Level up 
https://level-up.cc
Técnicas rudas
https://www.tecnicasrudas.org/
Asociación para el Progreso de las Comunicaciones
https://www.apc.org/es
Yo y mi sombra
https://myshadow.org/es
Protege.la
https://protege.la/
Security planner
https://securityplanner.org
Net Alert
https://netalert.me/es/
Seguridad holística (inglés)
https://holistic-security.tacticaltech.org
Kit de primeros auxilios digitales
https://www.digitalfirstaid.org/es/
Acoso online
https://acoso.online


### Ante emergencias
(Más info de cada una en: https://www.digitalfirstaid.org/es/support/)

Access Now
https://www.accessnow.org/help
Deflect
https://www.deflect.ca
Digital defenders
https://www.digitaldefenders.org
Front line defenders
https://www.frontlinedefenders.org/emergency-contact
Greenhost
https://greenhost.net
Huridocs
https://www.huridocs.org/how-we-help/